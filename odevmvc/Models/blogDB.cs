﻿namespace odevmvc.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class blogDB : DbContext
    {
        public blogDB()
            : base("name=blogDB")
        {
        }

        public virtual DbSet<etiket> etikets { get; set; }
        public virtual DbSet<kategori> kategoris { get; set; }
        public virtual DbSet<kullanıcı> kullanıcı { get; set; }
        public virtual DbSet<makaleler> makalelers { get; set; }
        public virtual DbSet<yetkilendirme> yetkilendirmes { get; set; }
        public virtual DbSet<yorum> yorums { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<etiket>()
                .HasMany(e => e.makalelers)
                .WithMany(e => e.etikets)
                .Map(m => m.ToTable("etiketmakale").MapLeftKey("etiketID").MapRightKey("makaleID"));

            modelBuilder.Entity<kategori>()
                .HasMany(e => e.makalelers)
                .WithRequired(e => e.kategori)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<kullanıcı>()
                .Property(e => e.Kullanıcı_Ad)
                .IsFixedLength();

            modelBuilder.Entity<kullanıcı>()
                .Property(e => e.sifre)
                .IsFixedLength();

            modelBuilder.Entity<kullanıcı>()
                .HasMany(e => e.makalelers)
                .WithRequired(e => e.kullanıcı)
                .HasForeignKey(e => e.Kullanıcı_ID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<kullanıcı>()
                .HasMany(e => e.yorums)
                .WithRequired(e => e.kullanıcı)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<makaleler>()
                .Property(e => e.Baslik)
                .IsFixedLength();

            modelBuilder.Entity<makaleler>()
                .HasMany(e => e.yorums)
                .WithRequired(e => e.makaleler)
                .HasForeignKey(e => e.MakaleID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<yetkilendirme>()
                .HasMany(e => e.kullanıcı)
                .WithRequired(e => e.yetkilendirme)
                .WillCascadeOnDelete(false);
        }
    }
}
