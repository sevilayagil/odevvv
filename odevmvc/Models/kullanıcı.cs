namespace odevmvc.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class kullanıcı
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public kullanıcı()
        {
            makalelers = new HashSet<makaleler>();
            yorums = new HashSet<yorum>();
        }

        public int ID { get; set; }

        [Required]
        [StringLength(10)]
        public string Kullanıcı_Ad { get; set; }

        [Required]
        [StringLength(10)]
        public string sifre { get; set; }

        internal object SingleOrDefault()
        {
            throw new NotImplementedException();
        }

        [StringLength(50)]
        public string isim { get; set; }

        [StringLength(50)]
        public string soyisim { get; set; }

        public int yetkiID { get; set; }

        [Column("kayıt tarihi", TypeName = "date")]
        public DateTime kayıt_tarihi { get; set; }

        public virtual yetkilendirme yetkilendirme { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<makaleler> makalelers { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<yorum> yorums { get; set; }
    }
}
