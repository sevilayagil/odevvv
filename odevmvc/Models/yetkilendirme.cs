namespace odevmvc.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yetkilendirme")]
    public partial class yetkilendirme
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public yetkilendirme()
        {
            kullanıcı = new HashSet<kullanıcı>();
        }

        [Key]
        public int yetkiID { get; set; }

        [StringLength(50)]
        public string yetkiAdı { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<kullanıcı> kullanıcı { get; set; }
    }
}
