namespace odevmvc.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("makaleler")]
    public partial class makaleler
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public makaleler()
        {
            yorums = new HashSet<yorum>();
            etikets = new HashSet<etiket>();
        }

        public int ID { get; set; }

        [Required]
        [StringLength(10)]
        public string Baslik { get; set; }

        [Required]
        public string icerik { get; set; }

        public int Kullanıcı_ID { get; set; }

        public DateTime Tarih { get; set; }

        public int KategoriID { get; set; }

        public virtual kategori kategori { get; set; }

        public virtual kullanıcı kullanıcı { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<yorum> yorums { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<etiket> etikets { get; set; }
    }
}
