namespace odevmvc.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("yorum")]
    public partial class yorum
    {
        public int ID { get; set; }

        [Required]
        public string Yorumİcerik { get; set; }

        public int KullanıcıID { get; set; }

        public int MakaleID { get; set; }

        public DateTime Tarih { get; set; }

        public virtual kullanıcı kullanıcı { get; set; }

        public virtual makaleler makaleler { get; set; }
    }
}
