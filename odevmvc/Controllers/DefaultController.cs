﻿using odevmvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace odevmvc.Controllers
{
    public class DefaultController : Controller
    {
        blogDB db = new blogDB();
        // GET: Default
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult login()
        {


            return View();
        }
        // bir atribute yazdık
        [HttpPost]
        public ActionResult login(kullanıcı model)
        {
            try
            {
                var varmi = db.kullanıcı.FirstOrDefault(i => i.Kullanıcı_Ad == model.Kullanıcı_Ad);


                if (varmi == null)
                {
                    return View();
                }
                if (varmi.sifre == model.sifre)
                {
                    Session["username"] = model.Kullanıcı_Ad;
                    return RedirectToAction("Index", "kullanıcı");
                }
                else
                {
                    return View();
                }
              
            }
            catch
            {
                return View();
            }
        }
        // GET: kullanici/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: kullanici/Create
        [HttpPost]
        public ActionResult Create(kullanıcı model)
        {
            try
            {


                var varmi = db.kullanıcı.Where(i => i.Kullanıcı_Ad == model.Kullanıcı_Ad).SingleOrDefault();
                if (varmi != null)
                {

                    return View();
                }
                if (string.IsNullOrEmpty(model.sifre))
                {
                    return View();
                }
                model.kayıt_tarihi = DateTime.Now;
                model.yetkiID = 1;
                db.kullanıcı.Add(model);
                db.SaveChanges();
                Session["username"] = model.Kullanıcı_Ad;
                return RedirectToAction("Index", "kullanıcı");
            }
            catch
            {
                return View();
            }
        }
    }
}