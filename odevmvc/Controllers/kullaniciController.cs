﻿using odevmvc.helpers;
using odevmvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace odevmvc.Controllers
{
    [Authorize]
    public class kullaniciController : yetkiliController
    {

        blogDB db = new blogDB();
       

        // GET: kullanici
        public ActionResult Index()
        {

            string kullanıcıadi = Session["username"].ToString();
            var kullanici = db.kullanıcı.Where(i => i.Kullanıcı_Ad == kullanıcıadi).SingleOrDefault();


            return View(kullanici);
        }

        // GET: kullanici/Details/5
        public ActionResult Details(int id)
        {
            var kisi = db.kullanıcı.Where(i => i.ID==id).SingleOrDefault();
            return View(kisi);
        }

        public ActionResult profil()
        {
            string Kullanıcı_Ad = Session["username"].ToString();
            var kisi = db.kullanıcı.Where(i => i.Kullanıcı_Ad==Kullanıcı_Ad).SingleOrDefault();
            return View(kisi);
        }


      

        // GET: kullanici/Edit/5

            //güvenlik açıgını kapamaya calıstım....
        public ActionResult Edit(int id)
        {
            string kullanıcıadı = Session["username"].ToString();
            var user = db.kullanıcı.Where(i => i.Kullanıcı_Ad == kullanıcıadı).SingleOrDefault();

            if (ortaksinif.EditDeketeIzınVarMı(id,user))
            {
                var kisi = db.kullanıcı.Where(i => i.ID == id).SingleOrDefault();
                return View(kisi);
            }
          
            return HttpNotFound();
           
        }

       

        // POST: kullanici/Edit/5
        //create mantıgı gibi az arguman kullandık...
        [HttpPost]
        public ActionResult Edit(int id, kullanıcı model)
        {
            try
            {
                // TODO: Add update logic here

                var kisi = db.kullanıcı.Where(i => i.ID == id).SingleOrDefault();
                kisi.isim = model.isim;
                kisi.soyisim = model.soyisim;
                kisi.sifre = model.sifre;
                db.SaveChanges();
                return RedirectToAction("Index","Default");
            }
            catch
            {
                return View();
            }
        }

        //çıkıs yapma
        public ActionResult logout ()
        {
            Session["username"] = null;
            return RedirectToAction("Index", "Default");
        }
        // GET: kullanici/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: kullanici/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
