﻿

using odevmvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace odevmvc.Controllers
{
   
    public class makaleController : yetkiliController
    {
        blogDB db = new blogDB();
        


        // GET: makale
        public ActionResult Index()
        {
            var makaleler = db.makalelers.ToList();
            return View(makaleler);
        }

        // GET: makale/Details/5
        public ActionResult Details(int id)
        {
            var makale = db.makalelers.Where(i => i.ID == id).SingleOrDefault();


            return View(makale);
        }
        public ActionResult kisimakalelistele()
        {
            var kullaniciadi = Session["username"].ToString();
            var makaleler = db.kullanıcı.Where(a => a.Kullanıcı_Ad == kullaniciadi).SingleOrDefault().makalelers.ToList();
            return View(makaleler);
        }
        // GET: makale/Create
        public ActionResult Create()
        {
            ViewBag.KategoriID = new SelectList(db.kategoris, "KategoriID", "kategoriad");
            return View();
        }

        // POST: makale/Create
        [HttpPost]
        public ActionResult Create(makaleler model)
        {
            try
            {
                //kullacıID ve tarih atama eksik oldugundan olustruduk
                string kullanıcıad = Session["username"].ToString();

                var kullanici = db.kullanıcı.Where(i => i.Kullanıcı_Ad == kullanıcıad).SingleOrDefault();
                return RedirectToAction("Index");
                model.Kullanıcı_ID = kullanici.ID;
                 model.Tarih = DateTime.Now;
                db.makalelers.Add(model);
                db.SaveChanges();
                return RedirectToAction("Index", "kullanıcı");
            }
            catch
            {
                return View();
            }
        }

        // GET: makale/Edit/5
        public ActionResult Edit(int id)
        {
            string kullaniciad=Session["username"].ToString();
            var makale = db.makalelers.Where(i => i.ID == id).SingleOrDefault();
            if(makale==null)
            {
                return HttpNotFound();
            }
            if (makale.kullanıcı.Kullanıcı_Ad==kullaniciad)
            {

                ViewBag.KategoriID = new SelectList(db.kategoris, "KategoriID", "kategoriad");
                return View(makale);
            }
            return HttpNotFound();
        }

        // POST: makale/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, makaleler model )
        {
            try
            {
                var makale = db.makalelers.Where(i => i.ID == id).SingleOrDefault();
                makale.Baslik = model.Baslik;
                makale.icerik = model.icerik;
                db.SaveChanges();
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: makale/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: makale/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
